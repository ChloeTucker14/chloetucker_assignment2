using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_EndGameUiTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Fire right thing at start
        FindObjectOfType<scp_ManagerUI>().LocateAndModifyEnemyKilledText();
    }
}
