using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_ManagerUI : MonoBehaviour
{

    [Header("Player's Variables")]
   // Slider that will represent player hp
    public Slider playerHpSlider;

    // Refrence to player life script
    scp_Player_LifeManager playerLife;

    [Header("Score Variables")]
    public Text enemiesKilledText;

    // Reference to game manager 
    private scp_Manager_Game gm;

    [Header("Death Screen Variables")]
    [SerializeField] GameObject enemyKilledTextObject;
    [SerializeField] Text enemyKilledTextComponent; 

    public static scp_ManagerUI UiInstance;

    private void Awake()
    {
        if (UiInstance != null)
        {
            Destroy(this.gameObject);
        }
        else if (UiInstance == null)
        {
            UiInstance = this;
        }

        DontDestroyOnLoad(this.gameObject);

    }

    // Start is called before the first frame update
    void Start()
    {
        // Finds script and stores in variable 
        playerLife = FindObjectOfType<scp_Player_LifeManager>();

        // Make max val of slider same as hp of player
        playerHpSlider.maxValue = playerLife.hp;

        // Showcase current life 
        playerHpSlider.value = playerLife.hp;

        // Finds scripts and fills variable 
        gm = FindObjectOfType<scp_Manager_Game>();
        // Will make enemies killed text be same as score
        enemiesKilledText.text = gm.enemiesKilled.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        // Makes slider UI equal current hp
        playerHpSlider.value = playerLife.hp;

        // Will make enemies killed text be same as score
        enemiesKilledText.text = gm.enemiesKilled.ToString();

    }

    public void LocateAndModifyEnemyKilledText()
    {
        // Will find the enemy killed game object
         enemyKilledTextObject = GameObject.Find("EnemyKilledNumber");
        // Will get text component and stores into variable 
        enemyKilledTextComponent = enemyKilledTextObject.GetComponent<Text>();
        // Updates the count 
        enemyKilledTextComponent.text = gm.enemiesKilled.ToString();
    
    }
}
