using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_ManagerAudio : MonoBehaviour
{
    // Reference to AudioSource
    public AudioSource source1;
    // Reference to sword sound 
    public AudioClip chainSound;

    public void ChainSound()
    {
        // Change Pitch randomly 
        source1.pitch = Random.Range(0.8f, 1.2f);
        // Will play chain sound
        source1.PlayOneShot(chainSound);
    }
}
