using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scp_Manager_Scene : MonoBehaviour
{
    public static scp_Manager_Scene sceneMan;

    private void Awake()
    {
        if (sceneMan != null)
        {
            Destroy(this.gameObject);
        }
        else if(sceneMan == null)
        {
            sceneMan = this;
        }

        DontDestroyOnLoad(this.gameObject);
            
    }
    public void LoadNextScene()
    {
        // Will load the next scene in the index
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadWinScreen()
    {
        SceneManager.LoadScene("scn_YouSurvived");
    }

    public void LoadDiedScreen()
    {
        SceneManager.LoadScene("scn_YouDied");
    }
}
