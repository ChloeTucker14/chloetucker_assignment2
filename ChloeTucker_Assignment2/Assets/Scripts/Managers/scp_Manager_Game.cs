using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_Game : MonoBehaviour
{
    // Variable to keep track of enemies killed
    public int enemiesKilled = 0;

    public static scp_Manager_Game gameManagerInstance;
    private void Awake()
    {
        if (gameManagerInstance != null)
        {
            Destroy(this.gameObject);
        }
        else if (gameManagerInstance == null)
        {
            gameManagerInstance = this;
        }

        DontDestroyOnLoad(this.gameObject);

    }

    public void EnemyKilledPlusOne()
    {
        // Add one to enemies killed variable
        enemiesKilled++;
    }
}
