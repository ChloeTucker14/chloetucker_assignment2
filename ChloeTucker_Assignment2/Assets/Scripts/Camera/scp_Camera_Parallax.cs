using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Camera_Parallax : MonoBehaviour
{
    private Transform cameraTransform;
    private Vector3 LastCameraPosition;

    [SerializeField] float parallaxMultiplier;
   
    // Start is called before the first frame update
    void Start()
    {
        // Find transform of camera and store into variable 
        cameraTransform = Camera.main.transform;
        // Make sure variable keeps track of position on start
        LastCameraPosition = cameraTransform.position;
    }

    private void LateUpdate()
    {
        // Direction 
        Vector3 deltaMove = cameraTransform.position - LastCameraPosition;

        // This will move the object to follow camera
        transform.position += deltaMove * parallaxMultiplier;

        // Make sure variable keeps track of position on late update
        LastCameraPosition = cameraTransform.position;


    }
}
