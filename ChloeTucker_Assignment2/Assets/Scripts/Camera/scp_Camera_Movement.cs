using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Camera_Movement : MonoBehaviour
{

    //Variable to store target of Camera
    public Transform target;

    // Variable to store offset to Camera
    public Vector3 cameraOffset;

    // Variable used by the lerp 
    public float smoothSpeed = 0.125f;

    private void FixedUpdate()
    {
        CameraFollow();
    }

    void CameraFollow()
    {
        // Going to store the desired position
        Vector3 desiredPosition = target.position + cameraOffset;

        // Will Lerp the position
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime); 

        // Will follow the player
        transform.position = smoothedPosition;
    }

}
