using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Movement : MonoBehaviour
{
    

    // Reference to the rigidbody
    private Rigidbody2D rb;

    // Will store direction of button press
    public float horizontalValue;

    // Variable controls how fast player moves
    public float speed;

    // Variable to deal with the flip
    private Vector2 flippedScale = new Vector2(-1f, 1);

    // Reference to player's inputs script
    scp_Player_Inputs inputs;
   

    // Start is called before the first frame update
    void Start()
    {
        // Finds the rigidbody and stores into rb
        rb = GetComponent<Rigidbody2D>();

        // Finds inputs script
        inputs = GetComponent<scp_Player_Inputs>();
    }

    void Update()
    {
        StoreHorizontalValue();
        MovePlayer();
    }

    void MovePlayer()
    {
        // Will create a vector 2 that we use to give force to player
        Vector2 force = new Vector2(horizontalValue * speed * Time.deltaTime, 0);

        if (rb != null)
        {
            // Will add force to rigidbody 
            rb.AddForce(force, ForceMode2D.Impulse);
        }

       

    }

    public void FlipThePlayer()
    {
        if (inputs.controls.Player.HorizontalMovement.ReadValue<float>() == -1)
        {
            transform.localScale = flippedScale;
        }

        else if (inputs.controls.Player.HorizontalMovement.ReadValue<float>() == 1)
        {
            transform.localScale = Vector2.one;
        }
    }

    void StoreHorizontalValue()
    {
        // Stores current float direction
        horizontalValue = inputs.controls.Player.HorizontalMovement.ReadValue<float>();
    }
}
