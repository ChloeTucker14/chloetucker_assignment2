using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_RealmPortal : MonoBehaviour
{
    //If touch portal i win
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PortalToNextLevel")
        {
            FindObjectOfType<scp_Manager_Scene>().LoadWinScreen();
        }
    }
}
