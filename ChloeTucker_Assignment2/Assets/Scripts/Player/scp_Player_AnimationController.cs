using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_AnimationController : MonoBehaviour
{
    // Variable to store reference to Animator
    private Animator anim;

    // Refrence to player's movement script
    private scp_Player_Movement movement;

    // Start is called before the first frame update
    void Start()
    {

        // Will find Animator and store into anim 
        anim = GetComponent<Animator>();

        // Will find script and store into variable 
        movement = GetComponent<scp_Player_Movement>();
        
    }

    // Update is called once per frame
    void Update()
    {
        MovementParameter();
    }

    void MovementParameter()
    {
        // Find absolute value of horizontalValue
        int absoluteValue = Mathf.Abs((int)movement.horizontalValue);

        // Sets parameter to be same as horizontalValue converted into integer
        anim.SetInteger("Movement", absoluteValue);
    }

    public void AttackA()
    {
        // Fires trigger for Attack anim
        anim.SetTrigger("AttackA");
    }

    public void Death()
    {
        anim.SetTrigger("Death");
    }

    public void Damaged()
    {
        anim.SetTrigger("Damaged");
    }
}
