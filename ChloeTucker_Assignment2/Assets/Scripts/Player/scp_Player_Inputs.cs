using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Inputs : MonoBehaviour
{
    // Variable that references the control scheme
    public scp_ControlScheme controls;

    // Reference to animation controller script
    private scp_Player_AnimationController animController;

    // Reference to Player's Movement script
    scp_Player_Movement playerMovement;

    // Reference to player attack logic 
    [SerializeField]scp_Player_Attack attackLogic;

    private void Awake()
    {

        // Create a new object of type control scheme
        controls = new scp_ControlScheme();

        // Will fire that method if we press left or right
        controls.Player.HorizontalMovement.performed += _ => OnHorizontalMovementPressed();
        // Reference to the Dash
        controls.Player.Dash.performed += _ => OnDash();
        // Reference to the Attack
        controls.Player.Attack.performed += _ => OnAttack();
        // Reference to the Charge
        controls.Player.Charge.performed += _ => OnCharge();

        // Finds animation controller script and stores into variable 
        animController = GetComponent<scp_Player_AnimationController>();

        // Find player movement script
        playerMovement = GetComponent<scp_Player_Movement>();

        // Find attack logic scrpit
        attackLogic = GetComponentInChildren<scp_Player_Attack>();
    }

    private void OnEnable()
    {
        // Enables the control script
        controls.Enable();
    }

    private void OnDisable()
    {
        // Disables the control script
        controls.Disable();
    }

  

    void OnHorizontalMovementPressed()
    {
     

        playerMovement.FlipThePlayer();
    }

    void OnDash()
    {

    }

    void OnAttack()
    {
        // Fires attack anim
        animController.AttackA();
        attackLogic.AttackLogic();
    }

    void OnCharge()
    {

    }

    
}
