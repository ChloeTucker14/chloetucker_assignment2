using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_LifeManager : MonoBehaviour
{
   // Variable to store life of player
   public int hp;
    // Reference to anim manager
    scp_Player_AnimationController animController;
    // Variable to store player status
    public bool isPlayerAlive = true;
    
    private void Start()
    {
        // Finds anim controller
        animController = GetComponent<scp_Player_AnimationController>();
    }
    public void RemoveHp(int numberOfHpToRemove)
    {
        hp -= numberOfHpToRemove;

        if (hp <= 0)
        {
            Death();
            
        }
        else
        {
            // Plays damaged anim
            animController.Damaged();
        }
    }

    public void Death()
    {
       // Plays death anim
        animController.Death();
        // Records that player is dead
        isPlayerAlive = false;
        // Deactivates colliders 
        GetComponent<Collider2D>().enabled = false;
        // Destroys rigidbody
        Destroy(GetComponent<Rigidbody2D>());
        //
        GetComponent<scp_Player_Inputs>().enabled = false;
        //Loads next scene after 2 seconds 
        StartCoroutine(DeathAnimationThenLoadDeathScene());
    
    
    }

    IEnumerator DeathAnimationThenLoadDeathScene()
    {
        yield return new WaitForSeconds(2f);
        // Gets the scene loader and fires loose method
        FindObjectOfType<scp_Manager_Scene>().LoadDiedScreen();
    }
}
