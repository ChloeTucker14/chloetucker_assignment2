using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class scp_Player_Attack : MonoBehaviour
{
    // Ball at end of chain
    public Transform overlapCirclePoint;

    // Radius of overlap circle 
    public float circleRadius;

    // Used to attack only enemies 
    public LayerMask enemyMask;

    // Variable to multiply kickback force 
    public float power;

    [Header("Screen Shake Variables")]
    public float shakeStrength;
    public float shakeDuration;

    // Reference to audio manager
    private scp_ManagerAudio audioMng;

    // Start is called before the first frame update
    void Start()
    {
        // Find Manager and store in variable
        audioMng = FindObjectOfType<scp_ManagerAudio>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttackLogic()
    {
        audioMng.ChainSound();
        
            //Debug.Log("Method is firing");
        // Returns all colliders in range of circle 
      Collider2D[] enemyColliders = Physics2D.OverlapCircleAll(overlapCirclePoint.position, circleRadius, enemyMask);

       // Go through each enemy collided with
        foreach (Collider2D enemy in enemyColliders)
        {
            Debug.Log(enemy.name);

            if (enemy.GetComponent<scp_Enemy_SorcererMaster>())
            {
                // Stores reference to sorcerer script
                var Sorcerer = enemy.GetComponent<scp_Enemy_SorcererMaster>();

                // Gives 1 damage to Sorcerer
                Sorcerer.GetDamaged(1);

                // Reference to sorcerer movement script 
                scp_Enemy_Sorcerer_Movement SorcerMove = enemy.GetComponent<scp_Enemy_Sorcerer_Movement>();
                // Starts the coroutine 
                StartCoroutine(SorcerMove.StopTheEnemyForSetAmountOfTime(2));

                // Give kickback to enemy
                Kickback(enemy.gameObject);

                // Shakes the camera 
                Camera.main.DOShakePosition(shakeDuration, shakeStrength);

            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
    }

    void Kickback(GameObject thingToKickBack)
    {
        // Get the direction of hit
        Vector3 direction = transform.position - thingToKickBack.transform.position;

        //We will get rigidbody of enemy
        Rigidbody2D enemyRb = thingToKickBack.GetComponent<Rigidbody2D>();

        // Then we add force to hit multiplied by a force multiplier
        enemyRb.AddForce(-direction * power, ForceMode2D.Impulse);

    }
}
