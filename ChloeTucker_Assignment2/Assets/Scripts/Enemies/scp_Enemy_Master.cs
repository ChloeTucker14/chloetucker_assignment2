using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Master : MonoBehaviour
{
    // Enemy lives
    public int lives;

    // Sprite we use for the enemy
    public Sprite enemySprite;

    // Used to define name of enemy
    public string enemyName;

    // Reference to game manager script
   protected scp_Manager_Game gm;

    protected virtual void Start()
    {
        // Finds the script
        gm = FindObjectOfType<scp_Manager_Game>();
    }

    public virtual void Death()
    {
        if (lives <= 0)
        {
            gm.EnemyKilledPlusOne();
        }
    }

    public virtual void GetDamaged(int numberOfLivesLost)
    {
        // Removes set amount of lives 
        lives -= numberOfLivesLost;


    }

    protected void SelfDestruct(float timeBeforeGettingDestroyed)
    {
        // Destroys enemy after set amount of time
        Destroy(this.gameObject, timeBeforeGettingDestroyed);
    }
}
