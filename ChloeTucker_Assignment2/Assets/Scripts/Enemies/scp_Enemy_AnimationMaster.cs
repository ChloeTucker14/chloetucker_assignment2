using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_AnimationMaster : MonoBehaviour
{
    // Reference to animator
    [SerializeField]protected Animator anim;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        // Find Animator and will store in anim
        anim = GetComponent<Animator>();   
    }

}
