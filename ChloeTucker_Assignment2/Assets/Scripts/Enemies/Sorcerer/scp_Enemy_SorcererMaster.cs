using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_SorcererMaster : scp_Enemy_Master
{
    // Reference to sorcerer anim script
    scp_Enemy_Sorcerer_AnimationManager Sorceranimation;

    // Reference to sorcerer movement script
    scp_Enemy_Sorcerer_Movement sorcerMove;
   
    protected override void Start()
    {
        base.Start();
        // Will find animation script
        Sorceranimation = GetComponent<scp_Enemy_Sorcerer_AnimationManager>();

        // Find movement script and store into variable 
        sorcerMove = GetComponent<scp_Enemy_Sorcerer_Movement>();
    }


    public override void Death()
    {
        base.Death();
        Sorceranimation.DeathAnimation();

        // Deactivates movement script
        GetComponent<scp_Enemy_Sorcerer_Movement>().enabled = false;

    }

    public override void GetDamaged(int numberOfLivesLost)
    {
        base.GetDamaged(numberOfLivesLost);

        // If lives >0 will fire get damaged anim
        if (lives > 0)
        {
            Sorceranimation.DamagedAnimation();
            // Changes color dependant on damage level
            Sorceranimation.Damage(lives);
        }
        else
        {
            Death();
            // Will stop enemy from moving 
            sorcerMove.DeathStop();
        }

        
       

    }

}
