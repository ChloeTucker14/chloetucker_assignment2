using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Sorcerer_Movement : MonoBehaviour
{
    // Reference to rigidbody 2D
    private Rigidbody2D sorcererRB;

    // Value to control speed
    public float speed;

    // Direction of enemy 
    public int direction = 1;

    // Reference to anim manager
    scp_Enemy_Sorcerer_AnimationManager sorcerAnim;

    // Variable to store original speed
    public float originalSpeed;

    //Bool to control if enemy can move 
    private bool canMove = true;

    // Start is called before the first frame update
    void Start()
    {
        // Finds the rigidbody
        sorcererRB = GetComponent<Rigidbody2D>();

        // Finds the anim manager
        sorcerAnim = GetComponent<scp_Enemy_Sorcerer_AnimationManager>();

        // Original speed
        originalSpeed = speed;
    }


    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            MoveSorcerer();
        }
        
    }

    void MoveSorcerer()
    {
        // Will create a vector 2 that we use to give force to enemy
        Vector2 force = new Vector2(direction * speed * Time.deltaTime, 0);

        // Will add force to rigidbody 
        sorcererRB.AddForce(force, ForceMode2D.Impulse);

        // Sets movement parameter
        sorcerAnim.SetMovementParameter((int)speed);

    }

    

   public IEnumerator FlipTheSorcerer()
    {
        yield return new WaitForSeconds(2f);

        // Reverse direction
        direction *= -1;
        
        // Gives speed to sorcerer
        speed = originalSpeed;
        // Flips the enemy
        transform.localScale = new Vector3(transform.localScale.x * -1, 1,1);
        


    }

    public IEnumerator StopTheEnemyForSetAmountOfTime(float time)
    {
        // Will stop movement
        canMove = false;
        // Change anim to idle 
        speed = 0;
        // Sets movement parameter
        sorcerAnim.SetMovementParameter((int)speed);
        yield return new WaitForSeconds(time);
        // Resets variable
        canMove = true;
        speed = originalSpeed;
        // Sets movement parameter
        sorcerAnim.SetMovementParameter((int)speed);
    }

    public void DeathStop()
    {
        canMove = false;
    }

    public void FlipInstantly()
    {
        // Reverse direction of movement
        direction *= -1;
        // Flips the enemy
        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        // Will stop movement
        canMove = false;
        // Change anim to idle 
        speed = 0;
        // Sets movement parameter
        sorcerAnim.SetMovementParameter((int)speed);

    }
}
