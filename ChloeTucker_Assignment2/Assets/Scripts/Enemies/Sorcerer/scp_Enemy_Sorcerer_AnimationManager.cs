using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Sorcerer_AnimationManager : scp_Enemy_AnimationMaster
{
    public Color damaged1;
    public Color damaged2;

    // Reference to sprite renderer
    [SerializeField]SpriteRenderer spriteR;

    protected override void Start()
    {
        base.Start();
        // Finds sprite render and stores into variable 
        spriteR = GetComponent<SpriteRenderer>();
    }
    public void AttackAnimation()
    {
        anim.SetTrigger("Attack");
    }

    public void DeathAnimation()
    {
        anim.SetTrigger("Death");
    }

    public void DamagedAnimation()
    {
        anim.SetTrigger("Damaged");
    }

    public void SetMovementParameter(int movement)
    {
        anim.SetInteger("Movement", movement);
    }

    public void Damage(int damageLevel)
    {
       
        switch (damageLevel)
        {
            case 2:
                Debug.Log("Case 2 firing");
                spriteR.material.SetColor("_Color", damaged1);
                break;
            case 1:
                Debug.Log("Case 1 firing");
                spriteR.material.SetColor("_Color", damaged2);
                 break;
        }
    }
}
