using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Sorcerer_AwarenessFront : MonoBehaviour
{
    // Reference to move script
   public scp_Enemy_Sorcerer_Movement sorcerMove;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("I can see the Player!");
            // Attack 

            // If enemy leaves range speed up for a bit

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Speed up for a bit 

            StartCoroutine(SpeedUpForASetAmountOfTime(3));
        }
    }

    IEnumerator SpeedUpForASetAmountOfTime(float time)
    {
        sorcerMove.speed = 120;
        yield return new WaitForSeconds(time);
        sorcerMove.speed = sorcerMove.originalSpeed;

    }
}
