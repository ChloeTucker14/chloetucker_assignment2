using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Sorcerer_WaypointLogic : MonoBehaviour
{
    // Reference to sorcerer move script 
    public scp_Enemy_Sorcerer_Movement sorcerMove;

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Waypoint" && gameObject.tag == "BodyTag")
        {
                // Stop
                sorcerMove.speed = 0;
            // 
            StartCoroutine(sorcerMove.FlipTheSorcerer());
        }
       
    }
}
