using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class scp_Enemy_Sorcerer_Attack : MonoBehaviour
{
    [Header("References")]
    // Reference to anim script
    public scp_Enemy_Sorcerer_AnimationManager sorcerAnim;
    [Header("Overlap Circle Variables")]
    // Ball at end of chain
    public Transform overlapCirclePoint;

    // Radius of overlap circle 
    public float circleRadius;

    // Used to attack only enemies 
    public LayerMask enemyMask;

    // Variable to control force of kickback
    public float power = 15f;

   


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Attacking Player");
            sorcerAnim.AttackAnimation();
            // Attacks the player
            AttackLogic();
        }
    }

    public void AttackLogic()
    {
        //Debug.Log("Method is firing");
        // Returns all colliders in range of circle 
        Collider2D playerColliders = Physics2D.OverlapCircle(overlapCirclePoint.position, circleRadius, enemyMask);
        
        if (playerColliders.GetComponent<scp_Player_LifeManager>())
        {
            // Removes 1 hp from Player 
            playerColliders.GetComponent<scp_Player_LifeManager>().RemoveHp(1);

            Kickback(playerColliders.gameObject);

           

        }
        
    }

    void Kickback(GameObject thingToKickBack)
    {
        // Get the direction of hit
        Vector3 direction = transform.position - thingToKickBack.transform.position;

        //We will get rigidbody of enemy
        Rigidbody2D enemyRb = thingToKickBack.GetComponent<Rigidbody2D>();

        // Then we add force to hit multiplied by a force multiplier
        enemyRb.AddForce(-direction * power, ForceMode2D.Impulse);

    }
}
