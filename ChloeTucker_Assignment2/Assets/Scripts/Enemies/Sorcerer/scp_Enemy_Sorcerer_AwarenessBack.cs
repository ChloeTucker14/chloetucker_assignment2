using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Sorcerer_AwarenessBack : MonoBehaviour
{
    // Reference to move script
    public scp_Enemy_Sorcerer_Movement sorcerMove;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Flip Me
            sorcerMove.FlipInstantly();
        }
    }
}
